#! /bin/bash

main() {
    uh="/usr/share"

    # Activate Canonical ppa + repositories update
    DS "Activate Canonical ppa + repositories update"
    checkS "sed -ie \"/# deb http:\/\/archive.canonical.com\/ubuntu\ /s/# //\" /etc/apt/sources.list"

    # Install favorite apps from apps_to_install.list and initial setup of libdvd-pkg
    DS "Install favorite apps from apps_to_install.list && initial setup of libdvd-pkg"
    DSBU "\n INSTALLATION IN PROGRESS. This may take some time..."
    progressSpin & # & for background process
    pS_pid=$!
    FAVS="$dir/RESOURCES/apps_to_install.list"
    DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends $(cat $FAVS) >/dev/null && checkL
    # checkCL "snap install codium --classic" # if one day you need vs-code alternative

    kill ${pS_pid}
    DSBU "INSTALLATION COMPLETED."
    # apt --fix-broken install && apt-get update && apt-get autoremove -y # better to use ftrace then fix-broken

    # Installing Microsoft fonts with .deb (sometimes required, avoid errors) && Editing dock apps
    DS "Installing Microsoft fonts && Editing dock apps"
    checkCL "echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections"
    override="99_launcher.favorites.gschema.override"
    checkCL "cp -r $dir/RESOURCES/${override} ${uh}/glib-2.0/schemas/${override}" "glib-compile-schemas $uh/glib-2.0/schemas/"

    # Changing background
    DS "Changing background"
    Bg="${uh}/backgrounds/backgroundCommown.jpg"
    Go="${uh}/glib-2.0/schemas/10_ubuntu-settings.gschema.override"
    checkCL "cp $dir/IMAGES/backgroundCommown.jpg $Bg" "cp -r $dir/RESOURCES/10_ubuntu-settings.gschema.override $Go"
    checkDF "-f" "$Bg" && checkDF "-f" "$Go" && checkCL "glib-compile-schemas ${uh}/glib-2.0/schemas/"
}

update() {
    DSBU "\n UPDATE IN PROGRESS. This may take some time..."
    progressSpin & # & for background process
    pS_pid=$!
    echo -e ${blue}'###                       [20%]\r'${rcolor}
    checkCL "apt-get update" "apt-get upgrade -y" # apt-get removes warnings (instead of apt)
    # if 'apt-get update' failed with exit code 100 check your internet connection
    # apt-get full-upgrade -y (not required)
    echo -e ${blue}'##############            [60%]\r'${rcolor}
    # Removes old kernels (keep one old and current)
    checkCL "apt-get autoremove -y" # check with : uname -r
    echo -e ${blue}'##########################[100%]\r'${rcolor}
    kill ${pS_pid}
    DSBU "UPDATE COMPLETED."
}

#! /bin/bash

# Original git repository of plymouth themes, for instance ubuntu-logo that we are using :
# https://git.launchpad.net/ubuntu/+source/plymouth/ , you can import project + clic mirror to fork :
# Commown FORK : https://gitlab.com/cgeissert/plymouth-commown-theme

# git clone https://gitlab.com/cgeissert/plymouth-commown-theme
# cd plymouth
# git switch --orphan commown-theme
# git commit --allow-empty -m "Initial commit on orphan branch"
# git push # git push --set-upstream origin test2
# git branch
# git checkout commown-theme -- themes/ubuntu-logo/

cipher() {
    # ADD IF ENCIPHERED
    # Enciphered password theme (use plymouthPwd.sh to test)
    DS "Enciphered password theme"
    pwdPrompt="s/Please unlock disk /Disque '/;s/: \"/' \| Veuillez saisir votre mot de passe.\"/"
    perl -pi -e "${pwdPrompt}" /lib/cryptsetup/functions                                                        # checkS non accepté car regex
    regexS="s/set up successfully/Disque déverouillé avec succès!/g"                                            # Success
    regexM="s/maximum number of tries exceeded/ Nombre d'essais maximum atteint! /g"                            # Max tries
    regexP="s/cryptsetup failed, bad password or options?/ Le déverrouillage a échoué, mauvais mot de passe /g" # Bad password or options
    # checkS non accepté car regex
    pathCR="/usr/share/initramfs-tools/scripts/local-top/cryptroot"
    checkS "perl -pi -e \"${regexS};${regexM};${regexP}\" ${pathCR}"
    apt-get install 'plymouth-theme*' -y >/dev/null
    # git clone https://gitlab.freedesktop.org/plymouth/plymouth.git
    pathT="/usr/share/plymouth/themes/"
    pathC="${pathT}commown/"
    exclude="!(ubuntu-logo.png|ubuntu-logo16.png|ubuntu-logo.grub)"
    checkS "shopt -s extglob" # enable extended globbing (for exclude)
    mkdir -p ${pathC} && cp -r ${pathT}ubuntu-logo/${exclude} ${pathC} && checkDF "-d" "${pathT}ubuntu-logo/"
    checkS "cp -r ./install/IMAGES/logoCommownR.png ${pathC}logoCommown.png"
    checkCL "mv ${pathC}ubuntu-logo.plymouth ${pathC}commown.plymouth" "mv ${pathC}ubuntu-logo.script ${pathC}commown.script"
    regexLF="s/(?<=logo_filename = \")(.*?)(?=\";)/logoCommown.png/gm"     # Image
    regexP="s/ubuntu-logo/commown/gm"                                      # Path
    regexN="s/Ubuntu Logo/Commown Logo/gm"                                 # Name
    regexD="s/(?<=Description=)(.*?)(?=\.)/A theme that features Commown/" # Description
    perl -pi -e "${regexLF}" ${pathC}commown.script >/dev/null && checkL   # checkS not valid with special regex
    checkS "perl -pi -e \"${regexP};${regexN};${regexD}\" ${pathC}commown.plymouth"
    checkS "update-alternatives --install ${pathT}default.plymouth default.plymouth ${pathC}commown.plymouth 100"
    checkS "cat ${pathC}commown.plymouth > ${pathT}default.plymouth" # cp -f works too
    # You can use this to try but not in a script : update-alternatives --config default.plymouth
    checkS "update-initramfs -u" # update initramfs for PLYMOUTH (after autoremove : Generating /boot/initrd.img)
}

#! /bin/bash

set -euo pipefail # throwErrors

source ./install/SCRIPTS/include.sh # color, display, check, etc.
source ./install/SCRIPTS/disk.sh    # extend disk, separate /home
source ./install/SCRIPTS/main.sh    # minimum installation
source ./install/SCRIPTS/boot.sh    # bootmanager / bootloader , refind/grub etc.
source ./install/SCRIPTS/cipher.sh  # cipher with plymouth

printf '\033[8;39;137t' # resize terminal
TC "${CYAN}" "${dir}"   # Title Commown
checkInit

# PROMPT User
vSeparate=0 && vExtend=0 && vMain=0 && vRefind=0 && vCipher=0
answer="" && answer2="" && answer3="" && answer4="" && answer5=""
if [ $vSeparate -eq 0 ]; then
    printf 'SEPARATE home (y/n)? ' && read answer
    if [ "$answer" != "${answer#[Yy]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        vSeparate=1
    fi
fi
if [ $vExtend -eq 0 ]; then
    printf 'EXTEND disk (y/n)? ' && read answer2
    if [ "$answer2" != "${answer2#[Yy]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        vExtend=1
    fi
fi
if [ $vMain -eq 0 ]; then
    printf 'Main (y/n)? ' && read answer3
    if [ "$answer3" != "${answer3#[Yy]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        vMain=1
    fi
fi
if [ $vRefind -eq 0 ]; then
    printf 'Refind (y/n)? ' && read answer4
    if [ "$answer4" != "${answer4#[Yy]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        vRefind=1
    fi
fi
if [ $vCipher -eq 0 ]; then
    printf 'LUKS theme (y/n)? ' && read answer5
    if [ "$answer5" != "${answer5#[Yy]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        vCipher=1
    fi
fi

if [[ vSeparate -eq 2 || vExtend -eq 2 || vMain -eq 2 || vRefind -eq 2 || vCipher -eq 2 ]]; then
    DS "\nYou chose ($vSeparate,$vExtend,$vMain,$vRefind,$vCipher) : \n - separate : $answer \n - extend : $answer2 \n - main : $answer3 \n - refind   : $answer4 \n - boot   : $answer5 \n"
    printf 'Do you wish to continue with your selected choices (y/n)? ' && read answerConfig
    if [ "$answerConfig" != "${answerConfig#[Nn]}" ]; then # "#[]", any Y or y in 1st position will be dropped.
        DSBU "You can relaunch the script. \n" && DS "Leaving. \n" && exit 0
    fi
fi

# PROMPT choice
pathP="./install/commownScript.sh" # prompt path
if [ $vSeparate -eq 1 ]; then
    sleep 1 && DSBU "\nSEPARATE START" && home                 # call home function in disk.sh : home
    regexP="s/vSeparate=0/vSeparate=2/;s/answer=/answer=DONE/" # Avoid redoing this block when restarting the script
    sleep 1 && checkS "perl -pi -e \"${regexP}\" ${pathP}" && DSBU "SEPARATE END \n"
fi
if [ $vExtend -eq 1 ]; then
    sleep 1 && DSBU "\nEXTEND START" && extend               # call extend function in disk.sh : extend
    regexP="s/vExtend=0/vExtend=2/;s/answer2=/answer2=DONE/" # Avoid redoing this block when restarting the script
    sleep 1 && checkS "perl -pi -e \"${regexP}\" ${pathP}" && DSBU "EXTEND END \n"
fi
if [ $vMain -eq 1 ]; then
    sleep 1 && DSBU "\nMAIN START" && main               # call main function in main.sh : main (apps & background)
    regexP="s/vMain=0/vMain=2/;s/answer3=/answer3=DONE/" # Avoid redoing this block when restarting the script
    sleep 1 && checkS "perl -pi -e \"${regexP}\" ${pathP}" && DSBU "MAIN END \n"
fi
if [ $vRefind -eq 1 ]; then
    sleep 1 && DSBU "\nREFIND START" && boot                 # call boot function in boot.sh : refind
    regexP="s/vRefind=0/vRefind=2/;s/answer4=/answer4=DONE/" # Avoid redoing this block when restarting the script
    sleep 1 && checkS "perl -pi -e \"${regexP}\" ${pathP}" && DSBU "REFIND END \n"
fi
if [ $vCipher -eq 1 ]; then
    sleep 1 && DSBU "\nCIPHER START" && cipher               # call cipher function in cipher.sh : plymouth
    regexP="s/vCipher=0/vCipher=2/;s/answer5=/answer5=DONE/" # Avoid redoing this block when restarting the script
    sleep 1 && checkS "perl -pi -e \"${regexP}\" ${pathP}" && DSBU "CIPHER END \n"
fi

# Updates
update # call update function in main.sh : update (kernel, apps, background)

# Reboot
TC "${GREEN}" "${dir}" # Title Commown

answerOR="" # OEM & reboot
echo -e ${GREEN}"TERMINÉ !"
DS "Désirez vous redémarrer immédiatement avec OEM (y)? / Ou le faire manuellement (n)? " && read answerOR
if [ "$answerOR" != "${answerOR#[Yy]}" ]; then
    DSBU "Redémarrage en cours, launcher OEM actif. \nRedémarrage dans : "
    # Timer 15s, delete install folder, launch oem, reboot :
    countdown 15
    if [[ "$PWD" != / ]]; then # if not in root, you are in session before launching oem
        checkS "gtk-launch oem-config-prepare-gtk.desktop"
    fi
    checkCL "rm -rf ${dir}" "reboot now"
else # if not y, you want to reboot manually
    DSBU "Veuillez lancer le launcher oem 'oem-config-prepare-gtk.desktop'. \nPuis redémarrez votre ordinateur."
fi
